/**
 * Three types of fake data can de declared:
 *  - {{ label }} refers to faker.io data (https://github.com/marak/Faker.js/)
 *  - [ item1, item2, item3 ] is custom data that we fake right here using an array
 *  - a specific label (i.e., 'id', 'hostname') means we perform a special handling to generate the fake data (see generate_neo4j_csv.js)
 */
const User = {
    id: 'id',
    firstName: '{{name.firstName}}',
    lastName: '{{name.lastName}}',
    userName: 'username',
    jobTitle: '{{name.jobTitle}}',
    department: ['Marketing', 'Security', 'Sales', 'Finance', 'Executive'],
    phone: '{{phone.phoneNumber}}',
    email: 'email',
    city: ['Quebec', 'Montreal', 'Levis', 'Trois-Riviere', 'Longueuil', 'Sainte-Agathe-de-Lotbiniere', 'Plessisville', 'Joliette'],
    state: ['Quebec'],
    country: ['Canada'],
    firstSeen: 'timestamp',
    lastSeen: 'timestamp'
}

const Host = {
    id: 'id',
    hostType: ['Workstation', 'Server', 'Laptop'],
    hostName: 'hostname',
    platform: ['Windows', 'Linux', 'Mac'],
    osVersion: 'osversion',
    sensorVersion: ['1.0.0', '1.1.0', '1.1.2'],
    externalIp: '{{internet.ip}}',
    localIp: '{{internet.ip}}',
    domain: ['company.com', 'company.net'],
    macAddress: '{{internet.mac}}',
    model: ['XPS-15', 'Pavilion-X360', 'Surface Book 2'],
    manufacturer: ['Dell', 'HP', 'Microsoft'],
    siteName: ['Complexe 1', 'Complexe 2', 'Complexe 3'],
    firstSeen: 'timestamp',
    lastSeen: 'timestamp'
}

const File = {
    id: 'id',
    fileName: '{{system.fileName}}',
    filePath: 'filepath',
    extension: 'extension',
    type: ['Microsoft Word Document', 'Microsoft Word Open XML Document', 'Rich Text Format File', 'Comma Separated Values File', 'PowerPoint Presentation', 'Portable Document Format File', 'Excel Spreadsheet', 'Hypertext Markup Language File'],
    sha1: 'sha1',
    sizeBytes: '{{random.number}}',
    created: 'timestamp',
    accessed: 'timestamp',
    modified: 'timestamp',
    deleted: 'deleted'
}

const Info = {
    id: 'id',
    ruleId: 'ruleid',
    hash: 'hash',
    keyId: 'keyid'
}

const UserInfo = {
    userId: 'userid',
    infoId: 'infoid',
    infoType: 'infotype',
    detected: 'timestamp'
}

const HostInfo = {
    hostId: 'hostid',
    infoId: 'infoid',
    infoType: 'infotype',
    detected: 'timestamp'
}

const FileInfo = {
    fileId: 'fileid',
    infoId: 'infoid',
    infoType: 'infotype',
    detected: 'timestamp'
}


module.exports = { User, Host, File, Info, UserInfo, HostInfo, FileInfo }