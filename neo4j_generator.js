const cliProgress = require('cli-progress')
const neo4j = require('neo4j-driver')
const faker = require('faker')
const path = require('path')
const sha1 = require('sha1')
const cluster = require('cluster')
const CsvFile = require('./csv_file.js')
const { User, Host, File, Info, UserInfo, HostInfo, FileInfo } = require('./data_model.js')

/** Neo4j */
const neo4j_url = 'neo4j://localhost:7687'
const neo4j_user = 'neo4j'
const password = 'qwerty'
const driver = neo4j.driver(neo4j_url, neo4j.auth.basic(neo4j_user, password))
const session = driver.session()

/** Faker.io */
faker.seed(2020) // For consistent results
faker.locale = 'en_CA'

/** Progress bar */
const progress = new cliProgress.SingleBar({
    clearOnComplete: false,
    hideCursor: true,
    etaBuffer: 10000
}, cliProgress.Presets.shades_grey)

/** CSV */
const csvWriter = new CsvFile()

/** Model configs */
const startDate = '2020-01-01'
const endDate = '2020-04-31'
const patternNumber = 1000 // Number of unique sensitive info patterns (i.e, credit card, ssn)

/**
 * Fake nodes setup
 *  - label:  the label for the .csv filename 
 *  - schema: the fake data schema imported form data_model.js
 *  - qty:    the number of desired entities
 *  - worker: the thread # (start at 1)
 */
const nodes = [
    { label: 'User', schema: User, qty: 25000, worker: '1' },
    { label: 'Host', schema: Host, qty: 50000, worker: '2' },
    { label: 'File', schema: File, qty: 100000, worker: '3' },
    { label: 'Info', schema: Info, qty: 250000, worker: '4' }
]

/**
 * Fake relationship setup
 *  - maxRelationship is the maximum number of relationship per sensitive info to Host/User/File
 *  - infoTypes are the fake types of info to be user in fake relationships
 */
const maxRelationship = 5
const infoTypes = ['Credit Card', 'Social Security Number', 'Passport Number', 'Account Number', 'Driver License']
const relationships = [
    { label: 'User-Info', schema: UserInfo },
    { label: 'Host-Info', schema: HostInfo },
    { label: 'File-Info', schema: FileInfo }
]

/**
 * Generates fake node data and automatically appends new lines to csv
 *  - this function is executed in the cluster worker scope
 * @param {obj} data the node data
 */
const createFakeNodeData = async (data) => {
    let lines = []
    for (i = 0; i < data.qty; i++) {
        lines.push(Object.keys(data.schema).reduce((entity, key) => {
            /** Custom fake data array handling */
            if (typeof (data.schema[key]) === 'object') {
                entity[key] = data.schema[key][Math.floor(Math.random() * data.schema[key].length)]
            }
            /** Other specific fake data handling */
            else {
                /** User specific */
                if (data.label === 'User') {
                    switch (data.schema[key]) {
                        case 'username':
                            entity[key] = `${entity['firstName']}.${entity['lastName']}`
                            break
                        case 'email':
                            entity[key] = `${entity['firstName']}.${entity['lastName']}@company.com`
                            break
                        default:
                            entity[key] = faker.fake(data.schema[key])
                    }
                }
                /** Host specific */
                else if (data.label === 'Host') {
                    switch (data.schema[key]) {
                        case 'hostname':
                            entity[key] = `${faker.fake('{{name.firstName}}')}-PC`
                            break
                        case 'osversion':
                            entity[key] = entity['platform'] == 'Windows' ? 'Windows 10 1803' : entity['platform'] == 'Linux' ? 'Ubuntu 18.04' : 'MacOs 10.15'
                            break
                        default:
                            entity[key] = faker.fake(data.schema[key])
                    }
                }
                /** File specific */
                else if (data.label === 'File') {
                    switch (data.schema[key]) {
                        case 'filepath':
                            const fakestuff = faker.fake('{{name.firstName}}, {{company.bsNoun}}, {{company.bsNoun}}').split(',')
                            entity[key] = `C:\\Users\\${fakestuff[0]}\\${fakestuff[1]}\\${fakestuff[2]}`
                            break
                        case 'extension':
                            entity[key] = entity['fileName'].split('.')[1]
                            break
                        case 'sha1':
                            entity[key] = sha1(entity['fileName'])
                            break
                        case 'deleted':
                            entity[key] = i % 100 === 0 ? new Date(faker.date.between(startDate, endDate)).toISOString() : ''
                            break
                        default:
                            entity[key] = faker.fake(data.schema[key])
                    }
                }
                /** Info specific */
                else if (data.label === 'Info') {
                    switch (data.schema[key]) {
                        case 'ruleid':
                            entity[key] = Math.floor(Math.random() * patternNumber)
                            break
                        case 'hash':
                            entity[key] = sha1(entity['ruleId'])
                            break
                        case 'keyid':
                            entity[key] = 'key_123' // dummy key
                            break
                        default:
                            entity[key] = faker.fake(data.schema[key])
                    }
                }
                /** Applicable to all */
                switch (data.schema[key]) {
                    case 'id':
                        entity[key] = `${data.label}-${i}`
                        break
                    case 'timestamp':
                        entity[key] = new Date(faker.date.between(startDate, endDate)).toISOString() //ISO8601 
                        break
                }
            }
            return entity
        }, {}))
        /** Append lines to csv for every 10% */
        if (i != 0 && (i % (data.qty * 0.10) === 0 || i === data.qty - 1)) {
            await appendtoCSV(data, lines)
            lines = []
        }
        /** Send update msg to master */
        process.send({ msgType: 'UPDATE', msg: 1 })
    }
}

/**
 * Creates relationships as per schema configs
 *  - Relationships from Info --> Host / File / User are generated in batches
 *  - Coherence of timestamps and ids
 */
const createRelationships = async () => {
    /** Convenience declarations */
    const user = nodes.find(node => node['label'] === 'User')
    const host = nodes.find(node => node['label'] === 'Host')
    const file = nodes.find(node => node['label'] === 'File')
    const info = nodes.find(node => node['label'] === 'Info')
    progress.start(info.qty, 0)
    for (i = 0; i < info.qty; i++) {
        /** Generate a relationship set for each Info */
        let num = Math.round(Math.random() * maxRelationship)
        num = num === 0 ? 1 : num // don't allow 0
        for (j = 0; j < num; j++) {
            /** Use same timestamp and infotype for a given set of X-Info relationship */
            const timestamp = new Date(faker.date.between(startDate, endDate)).toISOString() //ISO8601
            const infotype = infoTypes[Math.floor(Math.random() * infoTypes.length)]
            /** Create relationships based on schema */
            for (relationship of relationships) {
                const line = Object.keys(relationship.schema).reduce((entity, key) => {
                    /** Custom fake data array handling */
                    if (typeof (relationship.schema[key]) === 'object') {
                        entity[key] = relationship.schema[key][Math.floor(Math.random() * relationship.schema[key].length)]
                    }
                    /** Other specific fake data handling */
                    else {
                        /** User--Info specific */
                        if (relationship.schema === UserInfo) {
                            switch (relationship.schema[key]) {
                                case 'userid':
                                    entity[key] = `${user.label}-${Math.floor(Math.random() * user.qty)}`
                                    break
                                default:
                                    entity[key] = faker.fake(relationship.schema[key])
                            }
                        }
                        /** Host--Info specific */
                        else if (relationship.schema === HostInfo) {
                            switch (relationship.schema[key]) {
                                case 'hostid':
                                    entity[key] = `${host.label}-${Math.floor(Math.random() * host.qty)}`
                                    break
                                default:
                                    entity[key] = faker.fake(relationship.schema[key])
                            }
                        }
                        /** File--Info specific */
                        else if (relationship.schema === FileInfo) {
                            switch (relationship.schema[key]) {
                                case 'fileid':
                                    entity[key] = `${file.label}-${Math.floor(Math.random() * file.qty)}`
                                    break
                                default:
                                    entity[key] = faker.fake(relationship.schema[key])
                            }
                        }
                        /** Applicable to all */
                        switch (relationship.schema[key]) {
                            case 'infoid':
                                entity[key] = `${info.label}-${i}`
                                break
                            case 'infotype':
                                entity[key] = infotype
                                break
                            case 'timestamp':
                                entity[key] = timestamp
                                break
                        }
                    }
                    return entity
                }, {})
                await appendtoCSV(relationship, [line])
            }
        }
        progress.increment(1)
    }
}

/**
 * Appends lines to csv file
 *  - this function is executed in the cluster worker scope
 * @param {obj} node the node data
 * @param {obj} lines the new lines to be appended
 */
const appendtoCSV = async (node, lines) => {
    csvWriter.path = path.resolve(__dirname, `./output/${node.label}.csv`)
    csvWriter.headers = Array.from(Object.keys(node.schema))
    try {
        if (!node.csv) {
            await csvWriter.create(lines)
            node.csv = true
        } else
            await csvWriter.append(lines)
    } catch (error) {
        console.error(Error(`\nError: Appending to ${node.label}.csv: ${error.message}`))
    }
}

/**
 * Implements cluster to generate data in parallel
 * - each worker will generate the data and create the csv
 * - once csv is created, worker is killed and promise returns when all workers are done
 */
const dispatchFakers = async () => {
    progress.start(nodes.reduce((total, next) => total + (next['qty'] || 0), 0))
    return new Promise((resolve, reject) => {
        for (data of nodes) {
            const worker = cluster.fork({ workerId: data.worker, data: JSON.stringify(data) })
            worker.on('message', message => {
                switch (message.msgType) {
                    case 'UPDATE':
                        progress.increment(message.msg)
                        break
                    case 'END':
                        nodes[message.msg - 1]['finished'] = true
                        if (nodes.reduce((total, next) => total && next['finished'], true))
                            resolve()
                        break
                }
            })
            worker.on('error', reject)
            worker.on('exit', (code) => {
                if (code !== 0)
                    reject(new Error(`Worker ${worker.id} stopped with exit code ${code}`))
            })
        }
    })
}

/**
 * Entrypoint: this where the script starts
 */
if (cluster.isMaster) {
    /** Create nodes */
    console.info(`\nStep 1 - Generating fake nodes...\n`)
    dispatchFakers().then(() => {
        setTimeout(() => {
            console.info(`\n\nStep 2 - Generating fake relationships...\n`)
            createRelationships().then(async (result) => {
                setTimeout(() => {
                    console.info('\n\nDone!')
                    process.exit(0)
                }, 1000)
            })
        }, 1000)
    })
} else {
    const workerData = JSON.parse(process.env.data)
    const startTime = new Date()
    createFakeNodeData(workerData).then(() => {
        const endTime = new Date() - startTime
        process.send({ msgType: 'END', msg: workerData.worker })
        //console.info(`\nInfo: Worker ${workerData.worker} (${workerData.label}) killed (job time: ${(endTime / 1000).toFixed(0)} seconds)`)
        cluster.worker.kill()
    })
}

/**
 * TODO: This function could be used to check connection to neo4j
 */
const checkNeo4j = async () => {
    try {
        const result = await session.run(`call dbms.components() yield name, versions, edition unwind versions as version return name, version, edition`)
        console.info(`Connected to ${neo4j_url} - Neo4j version: ${result.records[0]._fields[1]}`)
        return true
    } catch (error) {
        console.error(`Unable to connect with ${neo4j_url}\n${error}`)
    }
}

/**
 * TODO: This function could be used to load the CSV files automatically
 */
const neo4jPopulate = async () => {
    try {
        const result = await session.run(
            `:auto USING PERIODIC COMMIT 1000
            LOAD CSV WITH HEADERS FROM 'file:///Users/jean5/Desktop/neo4jcode/output/User.csv' AS line
            CREATE (u:User{id: toInteger(line.id)})
            SET u=line`
        )
    } catch (error) {
        console.error(error)
    }
    finally {
        await session.close()
        await driver.close()
    }
}