# Neo4j Generator
The primary goal of this script is to automatically generate fake data and populate neo4j for testing and evaluation purposes.

The script performs the following steps:

1. Generates fake nodes:
    * Host, User, File and Info 
    * Each node is saved in a .csv file
    * Asynchronous using cluster

2. Generates fake relationships:
    * Host--Info, User--Info, and File--Info
      * TODO: Host--User, Host--File, User--File
    * Each relationship is saved in a .csv file
    
3. (TODO) Could load the fake data in neo4j
    * This requires configuration

# Getting Started
1. Run ```npm install``` to install required node modules

2. Inspect ```neo4j_generator.js``` and configure as desired

3. Settings are available to define number of Users, Hosts, etc.

4. Run ```node neo4j_generator.js``` to populate the DB with fake data

# Loading Fake Data Manually (Cypher)

1. Preparation:
  * In your neo4j configuration file, comment out ```dbms.directories.import=import``` to enable using any path for imports
  * Also check ```dbms.memory.heap.max_size``` to make sure you have enough memory allocated

2. Load nodes
  * Repeat for each csv file
  * Change URL and Labels as required

  Use this query to load a node from csv file (change file path):

```
:auto USING PERIODIC COMMIT 1000
LOAD CSV WITH HEADERS FROM 'file:///Users/jean5/Desktop/neo4jcode/output/User.csv' AS line
CREATE (u:User{id: toInteger(line.id)})
SET u=line;
```
3. Create Indexes
  * If you don't create indexes the next step of loading relationships will be very slow!
  * Make sure multi-query is enabled, when queries are seperated by a semicolon ```;```

Use these queries for creating the indexes:
```
CREATE INDEX FOR (n:User) ON (n.id);
CREATE INDEX FOR (n:Host) ON (n.id);
CREATE INDEX FOR (n:Info) ON (n.id);
CREATE INDEX FOR (n:File) ON (n.id);
```

4. Load relationships

Use this query to load relationships from csv file (change file path / do it for each file):
```
:auto USING PERIODIC COMMIT 10000
LOAD CSV WITH HEADERS FROM 'file:///Users/jean5/Desktop/neo4jcode/output/User-Info.csv' AS line
MATCH (i:Info{id: line.infoId}), (u:User{id: line.userId})
WITH line, i, u
CALL apoc.create.relationship(u, substring(line.detected, 0, 10), {infoType:line.infoType}, i) YIELD rel
RETURN count(rel)
```